/*
 * Dynamic Quick Searching 
 * As user types each key, the page displays search results (like Google) ;)
 * 
 * Parses a string to the SearchControl which then responds with a JSON Object.
 * Iterate through the JSON returned and display them.
 * With a delay of 500ms to let the database process in between function calls.
 */

var delay = null;
var item = [];
var itemIndex = 0;
var getSearchItem = 0;

$(document).ready(function() {

	$("body").click(function() {
		$(".pressedKey").css("display", "none");
	});
	$('#autoComplete').keyup(function() {
		clearTimeout(delay);

		delay = setTimeout(function() {
			dynamicSearch();
		}, 500);
	});

	$("#autoComplete").keydown(function() {
		$("#result").empty();
		$("#ContainerResult").empty();

	});

	$(document).on("click", ".pressedKey", function() {
		$("#autoComplete").val($(this).text());
	});
});

function dynamicSearch() {

	var itemName = $("#autoComplete").val();
	if (itemName != '') {

		$
		.ajax({
			type : "GET",
			url : "complete",
			data : "itemName=" + itemName,
			success : function(response) {
				item = response;

				for ( var i in response) {

					html = $("<ul class='items' style='list-style-type:none'>")
					.append(
					"<img src='/elec3609/resources/images/book.jpg' alt='book' width ='200px'> </img>")
					.append(
							"<li class='itemProperty'>"
							+ response[i].itemName
							+ "</li>")
							.append(
									$(
											"<form class='itemProperty' action='/elec3609/search' method='POST'>")
											.append(
													"<input type='hidden' class='newItem' name='myItem' value="
													+ response[i].itemName
													.trim()
													+ "></input>")
													.append(
													"<button class='btn btn-primary'  type='submit'>View Item</button>"));
					$("#ContainerResult").append(html);
					$("#result").append(
							'<p class = "pressedKey">'
							+ response[i].itemName + '</p>');
				}
			}
		});

	}

}