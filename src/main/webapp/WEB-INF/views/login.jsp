<%@ page import="java.io.*,java.util.*"%>
<%@ page session="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<body>

	<!-- Static navbar -->
	<nav class="navbar navbar-default navbar-static-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<c:url value="/home" />">Hyra</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="<c:url value="/register" />">Register</a></li>
					<li class="active"><a href="<c:url value="/login" />">Login</a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>



	<div class="container" style="width: 40%">	
		<form name="login" action="login.htm" method="post"
			class="form-signin" role="form">
			<h1 class="form-signin-heading">Sign In</h1>
			<br />
			<input type="text" class="form-control" name="username"
				placeholder="Username" required autofocus> <br /> <input
				type="password" class="form-control" name="password"
				placeholder="Password" required> <label class="checkbox">

				<input type="checkbox" value="remember-me"> Remember me
			</label>
			<button class="btn btn-lg btn-primary btn-block" type="submit">Sign In</button>
		</form>

	</div>
	${authenticationMessage}
</body>
</html>
