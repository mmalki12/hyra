<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page session="false"%>
<html>
<body>

	<!-- Static navbar -->
	<nav class="navbar navbar-default navbar-static-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<c:url value="/home" />">Hyra</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="<c:url value="/register" />">Register</a></li>
					<li><a href="<c:url value="/login" />">Login</a></li>
					<li class="active"><a href="<c:url value="/search" />">Search</a></li>
					<li><a href="<c:url value="/categories" />">View
							Categories</a></li>
					<li><a href="<c:url value="/checkout" />">View Checkout
							Cart</a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>

	<div class="container">
		Search for an item from our various list of items. (Try: Asus Charger)
		<br /> <br /> <input type="text" placeholder="Enter Item"
			id="autoComplete" />
		<h2>Item List</h2>

		<div id="result"></div>

		<br /> <br /> <br /> <br />
		<div id="ContainerResult"></div>
	</div>
</body>
</html>