<%@ page import="java.io.*,java.util.*"%>
<%@ page session="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<!-- Static navbar -->
<nav class="navbar navbar-default navbar-static-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<c:url value="/home" />">Hyra</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a href="<c:url value="/register" />">Register</a></li>
				<li><a href="<c:url value="/login" />">Login</a></li>
				<li><a href="<c:url value="/search" />">Search</a></li>
				<li class="active"><a href="<c:url value="/categories" />">View
						Categories</a></li>
				<li><a href="<c:url value="/checkout" />">View Checkout
						Cart</a></li>			
			</ul>
		</div>
		<!--/.nav-collapse -->
	</div>
</nav>

<div class="container">
<h1>Categories</h1>
<br />
<br />
<form action="categories" method="post">
	<input type="submit" value="Books" name="Books"></input> 
	<br />
	<br />
	<input type="submit" value="Phone Chargers" name="Phone Chargers"></input>
	<br />
	<br /> 
	<input type="submit" value="Laptop Chargers" name="Laptop Chargers"></input>
	<br />
	<br />
	<input type="submit" value="Before Exam" name="Before Exam"></input>
	<br />
	<br /> 
	<input type="submit" value="Electronic Equipment" name="Electronic Equipment"></input>		
</form>
</div>
</html>
