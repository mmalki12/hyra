<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>

<html>
<body>

	<!-- Static navbar -->
	<nav class="navbar navbar-default navbar-static-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<c:url value="/home" />">Hyra</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="<c:url value="/register" />">Register</a></li>
					<li><a href="<c:url value="/login" />">Login</a></li>
					<li><a href="<c:url value="/search" />">Search</a></li>
					<li><a href="<c:url value="/categories" />">View
							Categories</a></li>
					<li><a href="<c:url value="/checkout" />">View Checkout
							Cart</a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>


	<div class="container">

		<h1>Dashboard</h1>

		<p>Welcome to your Dashboard. From here you can view the
			Categories, Search for an item and view your checkout cart.</p>

		<br />


		<div class="list-group">
			<a href="categories.htm" class="list-group-item">View Categories</a>
			<a href="search.htm" class="list-group-item">Search</a> <a
				href="checkout.htm" class="list-group-item">Checkout Cart</a>
		</div>

	</div>

</body>

</html>
