<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<body>

<!-- Static navbar -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<c:url value="/home" />">Hyra</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="<c:url value="/register" />">Register</a></li>
            <li><a href="<c:url value="/login" />">Login</a></li>
            <li><a href="<c:url value="/search" />">Search</a></li>
            <li><a href="<c:url value="/categories" />">View Categories</a></li>
            <li><a href="<c:url value="/checkout" />">View Checkout Cart</a></li>            
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

	<p></p>
	<div class="container">
	<h3>Items</h3>
	<c:if test="${item.size()== 0}">
		<p>No Items are in this Category</p>
	</c:if>
	<c:forEach var="item" items="${item}">
		
		<i>Name: <c:out value="${item.itemName}" /></i><br/>
		<i>Description: <c:out value="${item.itemDescription}" /></i><br/>
		<i>Quantity Available: <c:out value="${item.quantityAvailable}" /></i><br/>
		<i>Total Quantity: <c:out value="${item.totalQuantity}" /></i><br/>
		<i>Rental price: <c:out value="${item.rentalPrice}" /></i><br/>
		<i>Purchase price: <c:out value="${item.purchasePrice}" /></i>
		<br></br>
		
	</c:forEach>
	</div>
	<p></p>

</body>
</html>