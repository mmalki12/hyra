<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Hyra</title>

<link href="<c:url value="/resources/css/bootstrap.css" />"
	rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/css/cover.css" />" rel="stylesheet"
	type="text/css" />
</head>

<body>

	<div class="site-wrapper">

		<div class="site-wrapper-inner">

			<div class="cover-container">

				<div class="masthead clearfix">
					<div class="inner">
						<h3 class="masthead-brand">Hyra</h3>
						<ul class="nav masthead-nav">							
            				<li><a href="<c:url value="/login" />">Login</a></li>           				
						</ul>
					</div>
				</div>

				<div class="inner cover">
					<h1 class="cover-heading">Revolutioninsing Hyring</h1>
					<p class="lead">Cover is a one-page template for building
						simple and beautiful home pages. Download, edit the text, and add
						your own fullscreen background photo to make it your own.</p>
					<p class="lead">
						<a href="<c:url value="/register" />" class="btn btn-lg btn-default">Register</a>
					</p>
				</div>

				<div class="mastfoot">
					<div class="inner">
						<p>
							Cover template for <a href="http://getbootstrap.com">Bootstrap</a>,
							by <a href="https://twitter.com/mdo">@mdo</a>.
						</p>
					</div>
				</div>

			</div>

		</div>

	</div>


	<script type="text/javascript"
		src="<c:url value="/resources/js/jquery.min.js" />"></script>
	<script type="text/javascript"
		src="<c:url value="/resources/js/bootstrap.min.js" />"> </script>
	<script type="text/javascript"
		src="<c:url value="/resources/js/docs.min.js" />"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script type="text/javascript"
		src="<c:url value="/resources/js/ie10-viewport-bug-workaround.js" />"> </script>
	<script type="text/javascript" src="/elec3609/resources/js/search.js"> </script>

</body>
</html>

