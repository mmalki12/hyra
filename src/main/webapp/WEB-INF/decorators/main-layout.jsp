<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Hyra</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="ELEC5619 2014sem2 Group Project">
<meta name="author" content="Group12">
<link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/css/navbar-static-top.css" />" rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/css/sticky-footer.css" />" rel="stylesheet" type="text/css" />
</head>

<body>
 
	<decorator:body />

	<div class="footer">
    	<c:import url="/WEB-INF/views/tags/footer.jsp" />
    </div>
	
	<script type="text/javascript" src="<c:url value="/resources/js/jquery.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"> </script>
	<script type="text/javascript" src="<c:url value="/resources/js/docs.min.js" />"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script type="text/javascript" src="<c:url value="/resources/js/ie10-viewport-bug-workaround.js" />"> </script>
	<script type="text/javascript" src="/elec3609/resources/js/search.js"> </script>
</body>

</html>