package au.usyd.elec3609.service;

import java.util.List;

import au.usyd.elec3609.domain.User;

public interface UserManager {
	public void addUser(User user);

	public List<User> getUsers();

	public void changePassword(User user);

	public User getUserById(long id);

	public void register(User user, String username, String password,
			String firstname, String lastname, String sex, String dateofbirth,
			String emailaddress);

	public boolean login(String username, String password);

	void logoff();

}
