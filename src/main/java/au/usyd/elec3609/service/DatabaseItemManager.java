package au.usyd.elec3609.service;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import au.usyd.elec3609.domain.Item;

/*
 * This class contains all the queries needed to dynamically alter the database or fetch necessary data.	
 */

@Service(value = "itemManager")
@Repository
@Transactional
public class DatabaseItemManager implements ItemManager {

	private SessionFactory sf;

	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sf = sf;
	}

	public Session getSession() {
		return sf.getCurrentSession();
	}

	@Override
	public void addItem(Item item) {
		getSession().save(item);

	}

	@Override
	public List<Item> getItems() {
		return ((List<Item>) getSession().createQuery("FROM Item").list());
	}

	@Override
	public Item getItemsById(long id) {
		return (Item) getSession().get(Item.class, id);
	}

	@Override
	public List<Item> getItemsByCategories(String category) {
		Criteria criteria = sf.getCurrentSession().createCriteria(Item.class);
		criteria.add(Restrictions.eq("category", category));
		return criteria.list();
	}

	@Override
	public List<Item> getItemsByPartialName(String name) {
		Criteria criteria = sf.getCurrentSession().createCriteria(Item.class);

		if (name != null) {

			criteria.add(Restrictions.ilike("itemname", name, MatchMode.START));
		}

		return criteria.list();
	}

	@Override
	public List<Item> getItemByName(String name) {
		Criteria criteria = sf.getCurrentSession().createCriteria(Item.class);

		criteria.add(Restrictions.ilike("itemname", name, MatchMode.START));
		criteria.setMaxResults(1);

		return criteria.list();

	}

}