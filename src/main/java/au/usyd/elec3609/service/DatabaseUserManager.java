package au.usyd.elec3609.service;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec3609.domain.User;

@Service(value = "userManager")
@Repository
@Transactional
public class DatabaseUserManager implements UserManager {

	private SessionFactory sf;

	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sf = sf;
	}

	public Session getSession() {
		return sf.getCurrentSession();
	}

	@Override
	public boolean login(String username, String password) {
		List<User> users = getUsers();
		// model.addAttribute("user", userManager.getUsers());
		int i;
		User userSpecific = new User();
		for (i = 0; i < users.size(); i++) {
			userSpecific = users.get(i);
			if (userSpecific.getUsername().equals(username)) {
				if (userSpecific.getPassword().equals(password)) {
					return true;
				}
			}
		}

		return false;

	}

	@Override
	public void register(User user, String username, String password,
			String firstname, String lastname, String sex, String dateofbirth,
			String emailaddress) {

		user.setUsername(username);
		user.setPassword(password);
		user.setFirstName(firstname);
		user.setLastName(lastname);
		user.setSex(sex);
		user.setDateOfBirth(dateofbirth);
		user.setEmailAddress(emailaddress);
		addUser(user);

	}

	@Override
	public void logoff() {
		// TODO Auto-generated method stub

	}

	@Override
	public void changePassword(User user) {
		getSession().merge(user);
	}

	@Override
	public User getUserById(long id) {
		return (User) getSession().get(User.class, id);
	}

	@Override
	public void addUser(User user) {
		getSession().save(user);
	}

	@Override
	public List<User> getUsers() {
		return (List<User>) getSession().createQuery("FROM User").list();
	}

}
