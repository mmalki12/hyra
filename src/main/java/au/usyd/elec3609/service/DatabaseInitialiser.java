package au.usyd.elec3609.service;

import javax.annotation.Resource;

import au.usyd.elec3609.domain.Item;
import au.usyd.elec3609.domain.User;

public class DatabaseInitialiser {
	@Resource(name = "itemManager")
	public ItemManager itemManager;

	@Resource(name = "userManager")
	public UserManager userManager;

	public void initialiseItemDatabase() {
		Item item1 = new Item();
		item1.setItemName("iPhone 5 Charger");
		item1.setCategory("Phone Chargers");
		item1.setItemDescription("Charger for Apple iPhone");
		item1.setQuantityAvailable(10);
		item1.setTotalQuantity(25);
		item1.setRentalPrice(2);
		item1.setImageFilePath("/elec3609/resources/images/charger.jpg");
		itemManager.addItem(item1);

		Item item2 = new Item();
		item2.setItemName("Software Engineering for Internet Applications");
		item2.setCategory("Books");
		item2.setItemDescription("A reference guide for software engineering students and also those pursuing courses on internet application and other related courses. This book is paperback version and is recommended by both professionals and also lecturers.");
		item2.setQuantityAvailable(20);
		item2.setTotalQuantity(1);
		item2.setRentalPrice(0.125);
		item2.setPurchasePrice(50);
		item2.setImageFilePath("/elec3609/resources/images/book.jpg");
		itemManager.addItem(item2);

		Item item3 = new Item();
		item3.setItemName("Laboratory Goggles");
		item3.setCategory("Electronic Equipment");
		item3.setItemDescription("A pair of plastic lab goggles. Highly durable and of the best quality");
		item3.setQuantityAvailable(20);
		item3.setTotalQuantity(5);
		item3.setRentalPrice(2.50);
		item3.setImageFilePath("/elec3609/resources/images/goggles.jpg");
		itemManager.addItem(item3);

		Item item4 = new Item();
		item4.setItemName("Calculator");
		item4.setCategory("Before Exam");
		item4.setItemDescription("CASIO 82AU (Recommended scientific calculator for all university quizzes and exams) ");
		item4.setQuantityAvailable(20);
		item4.setTotalQuantity(3);
		item4.setRentalPrice(1);
		item4.setImageFilePath("/elec3609/resources/images/calculator.png");
		itemManager.addItem(item4);

	}

	public void initialiseUserDatabase() {
		User user1 = new User();
		user1.setUsername("japp");
		user1.setPassword("oranges");
		user1.setFirstName("John");
		user1.setLastName("Appleseed");
		user1.setSex("Male");
		user1.setDateOfBirth("01/12/1992");
		user1.setEmailAddress("japp@uni.sydney.edu.au");
		userManager.addUser(user1);

		User user2 = new User();
		user2.setUsername("lpar");
		user2.setPassword("grapes");
		user2.setFirstName("Laura");
		user2.setLastName("Parker");
		user2.setSex("Female");
		user2.setDateOfBirth("05/07/1991");
		user2.setEmailAddress("lpar@uni.sydney.edu.au");
		userManager.addUser(user2);

		User user3 = new User();
		user3.setUsername("tjam");
		user3.setPassword("qwertylol");
		user3.setFirstName("Tom");
		user3.setLastName("James");
		user3.setSex("Male");
		user3.setDateOfBirth("06/12/1993");
		user3.setEmailAddress("tjam@uni.sydney.edu.au");
		userManager.addUser(user3);
	}

}
