package au.usyd.elec3609.service;

import java.util.List;

import au.usyd.elec3609.domain.Item;

public interface ItemManager {
	public void addItem(Item item);

	public List<Item> getItems();

	public Item getItemsById(long id);

	List<Item> getItemsByCategories(String category);

	List<Item> getItemsByPartialName(String name);

	List<Item> getItemByName(String name);
}