package au.usyd.elec3609.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/*
 * Item Class is a Plain Old Java Object which contains
 * all the database attributes. 
 * 
 * It contains all item table attributes the getters and the setters which will be used in the
 * view and also in the service to interact with the database. 
 */

@Entity
@Table(name = "Item")
public class Item implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "Id")
	private long id;

	@Column(name = "ItemName")
	private String itemname;

	@Column(name = "Category")
	private String category;

	@Column(name = "ItemDescription")
	private String itemdescription;

	@Column(name = "QuantityAvailable")
	private int quantityavailable;

	@Column(name = "TotalQuantity")
	private int totalquantity;

	@Column(name = "RentalPrice")
	private double rentalprice;

	@Column(name = "PurchasePrice")
	private double purchaseprice;

	@Column(name = "ImageFilePath")
	private String imageFilePath;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getItemName() {
		return itemname;
	}

	public void setItemName(String itemname) {
		this.itemname = itemname;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getItemDescription() {
		return itemdescription;
	}

	public void setItemDescription(String itemdescription) {
		this.itemdescription = itemdescription;
	}

	public int getQuantityAvailable() {
		return quantityavailable;
	}

	public void setQuantityAvailable(int quantityavailable) {
		this.quantityavailable = quantityavailable;
	}

	public int getTotalQuantity() {
		return totalquantity;
	}

	public void setTotalQuantity(int totalquantity) {
		this.totalquantity = totalquantity;
	}

	public double getRentalPrice() {
		return rentalprice;
	}

	public void setRentalPrice(double rentalprice) {
		this.rentalprice = rentalprice;
	}

	public double getPurchasePrice() {
		return rentalprice;
	}

	public void setPurchasePrice(double purchaseprice) {
		this.purchaseprice = purchaseprice;
	}

	public String getImageFilePath() {
		return imageFilePath;
	}

	public void setImageFilePath(String imageFilePath) {
		this.imageFilePath = imageFilePath;
	}

}
