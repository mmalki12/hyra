package au.usyd.elec3609.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/*
 * User Class is a Plain Old Java Object which contains
 * all the database attributes. 
 * 
 * It contains all the getters and the setters which will be used in the
 * view and also in the service to interact with the database. 
 */

@Entity
@Table(name = "User")
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "Id")
	private long id;

	@Column(name = "Username")
	private String username;

	@Column(name = "Password")
	private String password;

	@Column(name = "firstName")
	private String firstname;

	@Column(name = "lastName")
	private String lastname;

	@Column(name = "EmailAddress")
	private String emailaddress;

	@Column(name = "DateOfBirth")
	private String dateOfBirth;

	@Column(name = "Sex")
	private String sex;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstname;
	}

	public void setFirstName(String firstname) {
		this.firstname = firstname;
	}

	public String getLastName() {
		return lastname;
	}

	public void setLastName(String lastname) {
		this.lastname = lastname;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String string) {
		this.dateOfBirth = string;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getEmailAddress() {
		return emailaddress;
	}

	public void setEmailAddress(String emailaddress) {
		this.emailaddress = emailaddress;
	}

}
