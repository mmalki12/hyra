package au.usyd.elec3609.web;

import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.elec3609.domain.Item;
import au.usyd.elec3609.service.ItemManager;

@Controller
public class ViewItemController {
	private static final Logger logger = LoggerFactory
			.getLogger(HomeController.class);

	@Resource(name = "itemManager")
	private ItemManager itemManager;

	/**
	 * Show viewItem page
	 */

	@RequestMapping(value = "/viewItem", method = RequestMethod.GET)
	public String dashboard(Locale locale, Model model,
			HttpServletRequest httpServletRequest) {

		HttpSession session = httpServletRequest.getSession();

		String itemCategory = (String) session.getAttribute("categoryToVisit");

		model.addAttribute("item",
				itemManager.getItemsByCategories(itemCategory));
		return "viewItem";
	}

	@RequestMapping(value = "/viewItem", method = RequestMethod.POST)
	public String addToCart(HttpServletRequest httpServletRequest, Model model) {
		System.out.println(httpServletRequest.getParameter("item"));
		Long itemId = Long.valueOf(httpServletRequest.getParameter("item"))
				.longValue();

		Item item = itemManager.getItemsById(itemId);
		// item = (Item) itemObject;
		List<Item> tempList;
		HttpSession session = httpServletRequest.getSession();

		tempList = (List<Item>) session.getAttribute("ListOfItems");
		session.removeAttribute("ListOfItems");
		tempList.add(item);
		session.setAttribute("ListOfItems", tempList);
		if (item.getQuantityAvailable() > 0) {
			item.setQuantityAvailable(item.getQuantityAvailable() - 1);
			return "redirect:/checkout";
		}

		else {
			model.addAttribute("message", "Out of Stock");
			return "redirect:/viewItem";
		}

	}
}