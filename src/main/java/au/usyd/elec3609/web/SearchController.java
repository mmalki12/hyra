package au.usyd.elec3609.web;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import au.usyd.elec3609.domain.Item;
import au.usyd.elec3609.service.ItemManager;

@Controller
public class SearchController {

	private static final Logger logger = LoggerFactory
			.getLogger(SearchController.class);

	@Resource(name = "itemManager")
	private ItemManager itemManager;

	/**
	 * Simply selects the search view to render by returning its name.
	 */
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {

		model.addAttribute("listItem", itemManager.getItems());

		// model.addAttribute("Item", new Item());

		return "search";
	}

	@RequestMapping(value = "/complete", method = RequestMethod.GET)
	public @ResponseBody List<Item> autocomplete(
			@RequestParam(value = "itemName") String data, Model model) {
		return itemManager.getItemsByPartialName(data);

	}

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public String redirectToItem(@RequestParam("myItem") String itemName,
			HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();

		session.setAttribute("itemName", itemName);

		return "redirect:/searchItem";
	}

}