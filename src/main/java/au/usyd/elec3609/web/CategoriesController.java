package au.usyd.elec3609.web;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.elec3609.service.ItemManager;

@Controller
public class CategoriesController {
	private static final Logger logger = LoggerFactory
			.getLogger(HomeController.class);

	@Resource(name = "itemManager")
	private ItemManager itemManager;

	/**
	 * Show categories page
	 */
	@RequestMapping(value = "/categories", method = RequestMethod.GET)
	public String dashboard(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG,
				DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		model.addAttribute("serverTime", formattedDate);

		return "categories";
	}

	@RequestMapping(value = "/categories", method = RequestMethod.POST, params = "Books")
	public String getCategoryBooks(HttpServletRequest httpServletRequest) {

		HttpSession session = httpServletRequest.getSession();
		session.setAttribute("categoryToVisit",
				httpServletRequest.getParameter("Books"));

		return "redirect:/viewItem";

	}

	@RequestMapping(value = "/categories", method = RequestMethod.POST, params = "Phone Chargers")
	public String getCategoryPhChargers(HttpServletRequest httpServletRequest) {

		HttpSession session = httpServletRequest.getSession();
		session.setAttribute("categoryToVisit",
				httpServletRequest.getParameter("Phone Chargers"));

		return "redirect:/viewItem";

	}

	@RequestMapping(value = "/categories", method = RequestMethod.POST, params = "Electronic Equipment")
	public String getCategoryElectronic(HttpServletRequest httpServletRequest) {

		HttpSession session = httpServletRequest.getSession();
		session.setAttribute("categoryToVisit",
				httpServletRequest.getParameter("Electronic Equipment"));

		return "redirect:/viewItem";

	}

	@RequestMapping(value = "/categories", method = RequestMethod.POST, params = "Laptop Chargers")
	public String getCategoryLChargers(HttpServletRequest httpServletRequest) {

		HttpSession session = httpServletRequest.getSession();
		session.setAttribute("categoryToVisit",
				httpServletRequest.getParameter("Laptop Chargers"));

		return "redirect:/viewItem";

	}

	@RequestMapping(value = "/categories", method = RequestMethod.POST, params = "Before Exam")
	public String getCategoryBefore(HttpServletRequest httpServletRequest) {

		HttpSession session = httpServletRequest.getSession();
		session.setAttribute("categoryToVisit",
				httpServletRequest.getParameter("Before Exam"));

		return "redirect:/viewItem";

	}

}
