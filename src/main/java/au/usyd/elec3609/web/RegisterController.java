package au.usyd.elec3609.web;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import au.usyd.elec3609.domain.Item;
import au.usyd.elec3609.domain.User;
import au.usyd.elec3609.service.ItemManager;
import au.usyd.elec3609.service.UserManager;

@Controller
@SessionAttributes("User")
public class RegisterController {

	private static final Logger logger = LoggerFactory
			.getLogger(HomeController.class);

	@Resource(name = "userManager")
	UserManager userManager;

	@Resource(name = "itemManager")
	ItemManager itemManager;

	/**
	 * Show Register page
	 */
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String register(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG,
				DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		model.addAttribute("serverTime", formattedDate);
		return "register";
	}

	/**
	 * User has sent information for registration
	 */
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String submitRegistration(HttpServletRequest httpServletRequest) {

		User user = new User();
		userManager.register(user, httpServletRequest.getParameter("username"),
				httpServletRequest.getParameter("password"),
				httpServletRequest.getParameter("firstname"),
				httpServletRequest.getParameter("lastname"),
				httpServletRequest.getParameter("sex"),
				httpServletRequest.getParameter("dateofbirth"),
				httpServletRequest.getParameter("emailaddress"));

		HttpSession session = httpServletRequest.getSession();
		session.setAttribute("User", user);

		Item item1 = new Item();
		item1.setItemName("iPhone 5 Charger");
		item1.setCategory("Phone Chargers");
		item1.setItemDescription("Charger for Apple iPhone");
		item1.setQuantityAvailable(10);
		item1.setTotalQuantity(25);
		item1.setRentalPrice(2);
		item1.setImageFilePath("/src/main/webapp/resources/images/charger.jpg");
		itemManager.addItem(item1);

		Item item2 = new Item();
		item2.setItemName("Software Engineering for Internet Applications");
		item2.setCategory("Books");
		item2.setItemDescription("A reference guide for software engineering students and also those pursuing courses on internet application and other related courses. This book is paperback version and is recommended by both professionals and also lecturers.");
		item2.setQuantityAvailable(1);
		item2.setTotalQuantity(1);
		item2.setRentalPrice(0.125);
		item2.setPurchasePrice(50);
		item2.setImageFilePath("/src/main/webapp/resources/images/book.jpg");
		itemManager.addItem(item2);

		Item item3 = new Item();
		item3.setItemName("Laboratory Goggles");
		item3.setCategory("Electronic Equipment");
		item3.setItemDescription("A pair of plastic lab goggles. Highly durable and of the best quality");
		item3.setQuantityAvailable(5);
		item3.setTotalQuantity(5);
		item3.setRentalPrice(2.50);
		item3.setImageFilePath("/src/main/webapp/resources/images/goggles.jpg");
		itemManager.addItem(item3);

		Item item4 = new Item();
		item4.setItemName("Calculator");
		item4.setCategory("Before Exam");
		item4.setItemDescription("CASIO 82AU (Recommended scientific calculator for all university quizzes and exams) ");
		item4.setQuantityAvailable(1);
		item4.setTotalQuantity(3);
		item4.setRentalPrice(1);
		item4.setImageFilePath("/src/main/webapp/resources/images/calculator.png");
		itemManager.addItem(item4);

		Item item5 = new Item();
		item5.setItemName("Arduino");
		item5.setCategory("Electronic Equipment");
		item5.setItemDescription("Circuit Board");
		item5.setQuantityAvailable(5);
		item5.setTotalQuantity(5);
		item5.setRentalPrice(2.50);
		// item5.setImageFilePath();
		itemManager.addItem(item5);

		return "redirect:/login.htm";
	}
}