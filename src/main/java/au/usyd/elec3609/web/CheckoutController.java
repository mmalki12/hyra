package au.usyd.elec3609.web;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.elec3609.service.UserManager;

@Controller
public class CheckoutController {
	private static final Logger logger = LoggerFactory
			.getLogger(HomeController.class);

	@Resource(name = "userManager")
	private UserManager userManger;

	/**
	 * Show dashboard page
	 */
	@RequestMapping(value = "/checkout", method = RequestMethod.GET)
	public String dashboard(Locale locale, Model model,
			HttpServletRequest session) {
		logger.info("Welcome home! The client locale is {}.", locale);

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG,
				DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		model.addAttribute("item",
				session.getSession().getAttribute("ListOfItems"));

		return "checkout";
	}

	@RequestMapping(value = "/checkout", method = RequestMethod.POST)
	public String dashboard(Locale locale, Model model) {
		return "checkout";
	}

}