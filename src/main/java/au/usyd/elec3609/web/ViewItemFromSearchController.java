package au.usyd.elec3609.web;

import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.elec3609.service.ItemManager;

@Controller
public class ViewItemFromSearchController {
	private static final Logger logger = LoggerFactory
			.getLogger(HomeController.class);

	@Resource(name = "itemManager")
	private ItemManager itemManager;

	/**
	 * Show viewItem page
	 */

	@RequestMapping(value = "/searchItem", method = RequestMethod.GET)
	public String dashboard(Locale locale, Model model,
			HttpServletRequest httpServletRequest) {
		HttpSession session = httpServletRequest.getSession();

		String itemName = (String) session.getAttribute("itemName");

		System.out.println(itemName);
		model.addAttribute("item", itemManager.getItemByName(itemName));

		return "viewItemFromSearch";
	}
}