package au.usyd.elec3609.web;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.elec3609.service.UserManager;

@Controller
public class LoginController {

	private static final Logger logger = LoggerFactory
			.getLogger(HomeController.class);

	@Resource(name = "userManager")
	UserManager userManager;

	/**
	 * Simply selects the login view
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Locale locale, Model model,
			HttpServletRequest httpServletRequest) {

		logger.info("Welcome home! The client locale is {}.", locale);

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG,
				DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		HttpSession session = httpServletRequest.getSession();
		if (session.getAttribute("Authentication").equals("false")) {
			if (session.getAttribute("AttemptedLogin").equals("true")) {
				model.addAttribute("authenticationMessage",
						"Authentication failed");

			}

			model.addAttribute("authenticationMessage", "");

		}

		else {
			model.addAttribute("authenticationMessage", "You are logged in");

		}

		return "login";
	}

	/**
	 * Very basic verification of login username and password
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String verifyLogin(HttpServletRequest httpServletRequest, Model model) {
		HttpSession session = httpServletRequest.getSession();

		if (userManager.login(httpServletRequest.getParameter("username"),
				httpServletRequest.getParameter("password"))) {
			session.setAttribute("Authentication", "true");
			return "redirect:/dashboard";
		}

		else {
			session.removeAttribute("AttemptedLogin");
			session.setAttribute("AttemptedLogin", "true");
			model.addAttribute("authenticationMessage", "Authentication failed");
			return "login";
		}

	}

}
