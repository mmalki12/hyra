package au.usyd.elec3609.web;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.elec3609.domain.Item;
import au.usyd.elec3609.service.ItemManager;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	@Resource(name = "itemManager")
	public ItemManager itemManager;

	private static final Logger logger = LoggerFactory
			.getLogger(HomeController.class);

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String home(Locale locale, Model model,
			HttpServletRequest httpServletRequest) {
		logger.info("Welcome home! The client locale is {}.", locale);

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG,
				DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		model.addAttribute("serverTime", formattedDate);

		model.addAttribute("item", itemManager.getItems());
		HttpSession session = httpServletRequest.getSession();
		session.setAttribute("Authentication", "false");
		session.setAttribute("AttemptedLogin", "false");
		List<Item> itemList = new ArrayList<Item>();
		session.setAttribute("ListOfItems", itemList);
		return "home";
	}

}
